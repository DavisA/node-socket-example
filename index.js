console.log('starting server');

var express = require('express');
var app = express();
var server = app.listen(3000);
app.use(express.static('public'));

var socket = require('socket.io');

var io = socket(server);

io.sockets.on('connection', newConnection);

function newConnection(socket){
    console.log('New Connection: ' + socket.id);

    socket.on('mouse', mouseMsg);
    socket.on('clearCanvas', clearCanvas);

    function mouseMsg(data){
        socket.broadcast.emit('mouse', data); //this will emit to all clients except the sender
        //io.sockets.emit('mouse', data); // This will emit to ALL connected clients
        //console.log(data);

    }

    function clearCanvas(data){
        socket.broadcast.emit('clearCanvas', data);
        console.log('clear Cavas');

    }




}


