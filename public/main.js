const CANVAS_SIZE = 500;

var socket;

function setup(){
    createCanvas(CANVAS_SIZE, CANVAS_SIZE);
    background(50);


    socket = io.connect('http://localhost:3000/');
    socket.on('mouse', incomingDraw);
    socket.on('clearCanvas', clearCanvas);
}

function clearCanvas(data){
    background(51);

}

function incomingDraw(data){
    noStroke();
    fill(255,0,100);
    ellipse(data.x, data.y, 10, 10);
}



function draw(){
    //background(50);
    //ellipse(mouseX, mouseY, 50,50);

}

function keyPressed(){
    if(keyCode === 32){
        background(51);

        socket.emit('clearCanvas', true);   
   
    }
    
    

}

function mouseDragged() {
    noStroke();
    fill(255);
    ellipse(mouseX, mouseY, 10, 10);
    
    var data = {
        x: mouseX,
        y: mouseY
    }
    socket.emit('mouse', data);

    console.log('Sending mouse data: ', data.x + ',' + data.y);



    // prevent default
    return false;
  }