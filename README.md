A simple web sockets example i followed from Coding train.

Dependencies aren't in the repo.  To get npm to install the dependencies (express and socket.io):
$npm install

to run
$node index.js

Server listens on localhost:3000
web socket is hardcoded to connect to localhost
